-- Install pgcrypto extension
CREATE EXTENSION pgcrypto;

-- Install pg_stat_statements extension
CREATE EXTENSION pg_stat_statements;
