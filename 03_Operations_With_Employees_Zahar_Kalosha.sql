SELECT * FROM employees;

UPDATE employees SET last_name = 'Brown' WHERE email = 'jane.smith@example.com';

DELETE FROM employees WHERE email = 'john.doe@example.com';

SELECT * FROM employees;
