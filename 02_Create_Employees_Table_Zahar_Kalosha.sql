-- Create employees table with encrypted password column
CREATE TABLE employees (
  id SERIAL PRIMARY KEY,
  first_name VARCHAR(255),
  last_name VARCHAR(255),
  email VARCHAR(255),
  encrypted_password TEXT
);

-- Insert sample employee data with encrypted password
INSERT INTO employees (first_name, last_name, email, encrypted_password)
VALUES ('John', 'Doe', 'john.doe@example.com', crypt('password123', gen_salt('bf'))),
VALUES ('Oleg', 'Kastritsa', 'oleg.kastritsa@example.com', crypt('Kastrotsa_76', gen_salt('bf')));
VALUES ('Genadiy', 'Razmyslovich', 'genadiy.razmyslovich@example.com', crypt('Razmyslovich_71', gen_salt('bf')));
