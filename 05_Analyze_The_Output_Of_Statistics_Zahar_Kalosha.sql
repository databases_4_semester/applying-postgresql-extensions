-- Identify the most frequently executed queries
SELECT query, COUNT(*) AS calls
FROM pg_stat_statements
GROUP BY query
ORDER BY calls DESC;

-- Find queries with the highest total runtime
SELECT query,
       SUM(total_exec_time) OVER (PARTITION BY query) AS total_runtime,
       AVG(total_exec_time) OVER (PARTITION BY query) AS avg_runtime
FROM pg_stat_statements
ORDER BY total_runtime DESC;