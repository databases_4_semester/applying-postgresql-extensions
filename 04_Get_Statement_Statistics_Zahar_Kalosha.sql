--Configure the pg_stat_statements extension
ALTER SYSTEM SET shared_preload_libraries TO 'pg_stat_statements';
ALTER SYSTEM SET pg_stat_statements.track TO 'all';

SELECT * FROM pg_stat_statements;